﻿using System.Collections;
using System.Collections.Generic;
using LegendaryTreasure.Stats;
using UnityEngine;
using System.Linq;

namespace LegendaryTreasure.States
{
    [CreateAssetMenu(fileName = "State", menuName = "LegendaryTreasure/State")]
    public class State : ScriptableObject, IState
    {
        public Sprite Sprite => this.sprite;
        public string Story => this.story;
        public bool Repeatable => this.repeatable;
        public IEnumerable<IStateChoice> StateChoices => this.stateChoices;
        public IEnumerable<IStat> StatsModifiers { get => this.statsModifiers; set => this.statsModifiers = (List<Stat>)value; }
        public CharacterStatTypes CharacterStatType { get => this.characterStatType; set => this.characterStatType = value; }

        [SerializeField] protected Sprite sprite;
        [TextArea(3,5)] [SerializeField] protected string story;
        [SerializeField] protected bool repeatable;
        [SerializeField] protected List<StateChoice> stateChoices;
        [SerializeField] protected List<Stat> statsModifiers;

        protected CharacterStatTypes characterStatType;

        public bool HaveChoice()
        {
            if (this.stateChoices.Count() > 0)
            {
                return true;
            }
            return false;
        }

        public bool HaveImpact()
        {
            if (this.statsModifiers.Count() > 0)
            {
                return true;
            }
            return false;
        }
    }
}