﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LegendaryTreasure.States
{
    [Serializable]
    public class StatesManager : IStatesManager
    {
        public IEnumerable<IState> AvailableStates => this.availableStates;
        public IEnumerable<IState> UnavailableStates => this.unavailableStates;
        public IState IntroductionState => this.introductionState;

        [SerializeField] protected List<State> availableStates;
        [SerializeField] protected List<State> unavailableStates;
        [SerializeField] protected State introductionState;

        public IState GetRandomState()
        {
            int count = this.availableStates.Count;
            int randomStateIndex = UnityEngine.Random.Range(0, this.availableStates.Count);
            return this.availableStates[randomStateIndex];
        }

        public IState GetNextState()
        {
            if (this.availableStates.Count < 1)
            {
                this.RefillStates();
            }
            IState nextState = this.GetRandomState();
            this.SetUnavailable((State)nextState);
            return nextState;
        }

        public void RefillStates()
        {
            foreach (var state in this.unavailableStates)
            {
                this.availableStates.Add(state);
            }
            this.ClearUnavailable();
        }

        public void ClearUnavailable()
        {
            this.unavailableStates.Clear();
        }

        public void SetUnavailable(State state)
        {
            if (state.Repeatable)
            {
                this.unavailableStates.Add(state);
            }
            this.availableStates.Remove(state);
        }
    }
}