﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LegendaryTreasure.Stats;

namespace LegendaryTreasure.States
{
    public interface IState
    {
        Sprite Sprite { get; }
        string Story { get; }
        bool Repeatable { get; }
        IEnumerable<IStateChoice> StateChoices { get; }
        IEnumerable<IStat> StatsModifiers { get; set; }
        CharacterStatTypes CharacterStatType { get; set; }
        bool HaveChoice();
        bool HaveImpact();
    }
}