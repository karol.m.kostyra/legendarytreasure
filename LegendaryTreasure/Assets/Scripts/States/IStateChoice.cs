﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.States
{
    public interface IStateChoice
    {
        string Description { get; }
        IState NextState { get; }
    }
}