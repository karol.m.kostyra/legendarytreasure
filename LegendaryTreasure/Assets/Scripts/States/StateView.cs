﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace LegendaryTreasure.States
{
    public class StateView : MonoBehaviour, IStateView
    {
        public IStatesManager StatesManager => this.statesManager;
        public GameObject ChoiceButtonsHandler => this.choiceButtonsHandler;
        public GameObject ButtonPrefab => this.buttonPrefab;
        public Button NewStateButton => this.newStateButton;
        public Image StateImage => this.stateImage;
        public Text StateStory => this.stateStory;
        public Text StateImpact => this.stateImpact;
        public State StateToDisplay => this.stateToDisplay;

        [SerializeField] protected StatesManager statesManager;
        [SerializeField] protected GameObject choiceButtonsHandler;
        [SerializeField] protected GameObject buttonPrefab;
        [SerializeField] protected Button newStateButton;
        [SerializeField] protected Image stateImage;
        [SerializeField] protected Text stateStory;
        [SerializeField] protected Text stateImpact;

        protected State stateToDisplay;

        private void Start()
        {
            this.Initialize();
            this.stateToDisplay = (State)this.statesManager.IntroductionState;
            this.DisplayStateData();
        }

        public void Initialize()
        {
            this.stateImage.gameObject.SetActive(true);
            this.stateStory.gameObject.SetActive(true);
            this.stateImpact.text = "";
            this.newStateButton.gameObject.SetActive(true);
            this.choiceButtonsHandler.SetActive(false);
        }

        public void SetNextState()
        {
            this.stateToDisplay = (State)this.statesManager.GetNextState();
            this.newStateButton.gameObject.SetActive(false);
            this.choiceButtonsHandler.SetActive(true);
            this.DisplayStateData();
            this.PrepareChoiceButtons();
        }

        public void DisplayStateData()
        {
            this.stateStory.text = this.stateToDisplay.Story;
            this.stateImage.sprite = this.stateToDisplay.Sprite;
            this.stateImpact.text = "";
            if (this.stateToDisplay.HaveImpact())
            {
                this.ViewResult();
            }
        }

        public void ViewResult()
        {
            string result = "";
            foreach (var item in this.stateToDisplay.StatsModifiers)
            {
                string symbol = item.Amount > 0 ? " +" : " ";
                result += item.StatType.Name + ":" + symbol + item.Amount.ToString() + "% ";
            }
            this.stateImpact.text += "\n\n{ " + result + "}";
        }

        public void PrepareChoiceButtons()
        {
            if(!this.stateToDisplay.HaveChoice())
            {
                this.newStateButton.gameObject.SetActive(true);
                this.choiceButtonsHandler.SetActive(false);
            }

            this.ClearChoiceButtons();
            int index = 1;

            foreach(var choice in this.stateToDisplay.StateChoices)
            {
                GameObject button = Instantiate(this.buttonPrefab, this.choiceButtonsHandler.transform);
                button.GetComponent<Button>().onClick.AddListener(() => { this.stateToDisplay = (State)choice.NextState;
                                                                          this.DisplayStateData();
                                                                          this.PrepareChoiceButtons(); });

                button.GetComponentInChildren<Text>().text = index.ToString() + ") " + choice.Description;
                index++;
            }
        }

        private void ClearChoiceButtons()
        {
            foreach (Transform button in this.choiceButtonsHandler.transform)
            {
                GameObject.Destroy(button.gameObject);
            }
        }
    }
}