﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.States
{
    [Serializable]
    public class StateChoice : IStateChoice
    {
        public string Description => this.description;
        public IState NextState => this.nextState;

        [TextArea(3,5)] [SerializeField] protected string description;
        [SerializeField] protected State nextState;
    }
}