﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LegendaryTreasure.States
{
    public interface IStatesManager
    {
        IEnumerable<IState> AvailableStates { get; }
        IEnumerable<IState> UnavailableStates { get; }
        IState IntroductionState { get; }
        IState GetRandomState();
        IState GetNextState();
        void RefillStates();
        void SetUnavailable(State state);
        void ClearUnavailable();
    }
}