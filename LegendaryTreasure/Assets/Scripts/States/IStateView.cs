﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LegendaryTreasure.States
{
    public interface IStateView
    {
        IStatesManager StatesManager { get; }
        GameObject ChoiceButtonsHandler { get; }
        GameObject ButtonPrefab { get; }
        Button NewStateButton { get; }
        Image StateImage { get; }
        Text StateStory { get; }
        Text StateImpact { get; }
        State StateToDisplay { get; }
        void Initialize();
        void SetNextState();
        void ViewResult();
        void PrepareChoiceButtons();
        void DisplayStateData();
    }
}