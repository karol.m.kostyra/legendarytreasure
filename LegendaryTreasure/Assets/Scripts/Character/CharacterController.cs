﻿using LegendaryTreasure.States;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Characters
{
    public class CharacterController : MonoBehaviour
    {
        public ICharacter Character => this.character;

        [SerializeField] protected Character character;
    }
}