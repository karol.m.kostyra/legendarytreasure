﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LegendaryTreasure.Stats;
using LegendaryTreasure.States;

namespace LegendaryTreasure.Characters
{
    public interface ICharacter
    {
        ICharacterStats CharacterStats { get; }
        IState StartingState { get; }
        IStatsManager StatsManager { get; }
    }
}