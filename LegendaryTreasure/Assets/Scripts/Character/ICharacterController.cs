﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Characters
{
    public interface ICharacterController
    {
        ICharacter Character { get; }
    }
}