﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LegendaryTreasure.Stats;
using LegendaryTreasure.States;
using System.Linq;
using System;

namespace LegendaryTreasure.Characters
{
    [Serializable]
    public class Character : ICharacter
    {
        public ICharacterStats CharacterStats => this.characterStats;
        public IState StartingState => this.startingState;
        public IStatsManager StatsManager => this.statsManager;

        [SerializeField] protected CharacterStats characterStats;
        [SerializeField] protected State startingState;
        protected StatsManager statsManager = new StatsManager();
    }
}