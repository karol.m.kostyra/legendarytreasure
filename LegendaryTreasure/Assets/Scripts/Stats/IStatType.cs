﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Stats
{
    public interface IStatType
    {
        string Name { get; }
        string Description { get; }
        Sprite Image { get; }
    }
}