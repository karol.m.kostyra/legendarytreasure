﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using LegendaryTreasure.States;

namespace LegendaryTreasure.Stats
{
    public class StatsManager : IStatsManager
    {
        public void UpdateCharacterStats(IEnumerable<IStat> characterStats, IEnumerable<IStat> statsModifiers)
        {
            foreach (var stat in characterStats)
            {
                IStat statModifier = statsModifiers.FirstOrDefault(t => t.StatType == stat.StatType);
                if (statModifier != null)
                {
                    stat.Amount += statModifier.Amount;
                }
            }
        }
    }
}