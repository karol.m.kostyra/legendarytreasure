﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Stats
{
    public interface ICharacterStats
    {
        IEnumerable<IStat> Stats { get; }
    }
}