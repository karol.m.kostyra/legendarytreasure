﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Stats
{
    public interface IStat
    {
        IStatType StatType { get; set; }
        int Amount { get; set; }
    }
}