﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Stats
{
    [CreateAssetMenu(fileName = "Stat", menuName = "LegendaryTreasure/Stats/Stat")]
    public class StatType : ScriptableObject, IStatType
    {
        public string Name => this.name;
        public string Description => this.description;
        public Sprite Image => this.image;

        [SerializeField] protected new string name;
        [SerializeField] protected string description;
        [SerializeField] protected Sprite image;
    }
}