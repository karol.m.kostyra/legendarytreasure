﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LegendaryTreasure.States;

namespace LegendaryTreasure.Stats
{
    public enum CharacterStatTypes
    {
        Captain,
        Slave,
        Programmer,
        Tester
    }

    [CreateAssetMenu(fileName = "CharacterStats", menuName = "LegendaryTreasure/Stats/CharacterStats")]
    public class CharacterStats : ScriptableObject, ICharacterStats
    {
        public CharacterStatTypes CharacterType;
        public IEnumerable<IStat> Stats => this.stats;

        [SerializeField] protected List<Stat> stats;
    }
}