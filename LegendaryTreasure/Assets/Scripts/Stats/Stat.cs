﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LegendaryTreasure.Stats
{
    [Serializable]
    public class Stat : IStat
    {
        public IStatType StatType { get => this.statType; set => this.statType = (StatType)value; }
        public int Amount { get => this.amount; set => this.amount = value; }

        [SerializeField] protected StatType statType;
        [SerializeField] protected int amount;
    }
}