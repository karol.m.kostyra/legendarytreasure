﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LegendaryTreasure.States;

namespace LegendaryTreasure.Stats
{
    public interface IStatsManager
    {
        void UpdateCharacterStats(IEnumerable<IStat> characterStats, IEnumerable<IStat> statsModifiers);
    }
}