﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using LegendaryTreasure.Stats;

namespace LegendaryTreasure.States
{
#if UNITY_EDITOR
    [CustomEditor(typeof(State))]
    class CustomState : Editor
    {
        private State state;
        private CharacterStatTypes newCharacterStatType;
        private string statsTypeTooltip = "List of available character stats types.\n" +
            "(Make sure that directory 'CharacterStats' which containing the stats types is in 'Resources' directory)";

        public override void OnInspectorGUI()
        {
            base.DrawDefaultInspector();

            this.state = (State)target;
            
            GUILayout.Space(15);
            EditorGUI.DrawRect(EditorGUILayout.GetControlRect(false, 1), Color.gray);
            GUILayout.Space(5);

            
            this.newCharacterStatType = (CharacterStatTypes)EditorGUILayout.EnumPopup(new GUIContent("Character stat type: ", this.statsTypeTooltip), this.state.CharacterStatType);

            if (this.newCharacterStatType != this.state.CharacterStatType)
            {
                this.state.CharacterStatType = this.newCharacterStatType;
                GetCharacterStats(this.state);
            }
        }

        private void SetStatsModifiers(IState state, ICharacterStats statsType)
        {
            List<Stat> statsList = new List<Stat>();
            foreach (var stat in statsType.Stats)
            {
                Stat newStat = PrepareStatInstance(stat);
                statsList.Add(newStat);
            }
            state.StatsModifiers = statsList.AsEnumerable();
        }

        private Stat PrepareStatInstance(IStat stat)
        {
            Stat newStat = new Stat();
            newStat.StatType = stat.StatType;
            return newStat;
        }

        private void GetCharacterStats(IState state)
        {
            CharacterStats[] characterStatTypes = Resources.LoadAll("CharacterStats", typeof(CharacterStats)).Cast<CharacterStats>().ToArray();

            foreach (var statsType in characterStatTypes)
            {
                if(statsType.CharacterType == state.CharacterStatType)
                {
                    SetStatsModifiers(state, statsType);
                }
            }
        }
    }
#endif
}