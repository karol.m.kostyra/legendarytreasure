﻿using System.Collections;
using System.Collections.Generic;
using LegendaryTreasure.States;
using LegendaryTreasure.Stats;
using LegendaryTreasure.Characters;
using NUnit.Framework;
using NSubstitute;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;

namespace Tests
{
    public class StatsTest
    {
        [Test]
        public void CanIncrementCharacterStats()
        {
            //Arrange
            StatsManager statsManager = new StatsManager();
            Stat baseStat = new Stat();
            Stat statModifier = new Stat();
            IStatType statType = ScriptableObject.CreateInstance<StatType>();
            List<Stat> characterStats = new List<Stat>();
            List<Stat> statModifiers = new List<Stat>();

            statModifier.StatType = statType;
            baseStat.StatType = statType;
            statModifier.Amount = 25;
            baseStat.Amount = 50;
            characterStats.Add(baseStat);
            statModifiers.Add(statModifier);

            //Act
            statsManager.UpdateCharacterStats(characterStats, statModifiers);

            //Assert
            Assert.AreEqual(75, characterStats.FirstOrDefault(t => t.StatType == statType).Amount);
        }

        [Test]
        public void CanDecreaseCharacterStats()
        {
            //Arrange
            StatsManager statsManager = new StatsManager();
            Stat baseStat = new Stat();
            Stat statModifier = new Stat();
            IStatType statType = ScriptableObject.CreateInstance<StatType>();
            List<Stat> characterStats = new List<Stat>();
            List<Stat> statModifiers = new List<Stat>();

            statModifier.StatType = statType;
            baseStat.StatType = statType;
            statModifier.Amount = -25;
            baseStat.Amount = 50;
            characterStats.Add(baseStat);
            statModifiers.Add(statModifier);

            //Act
            statsManager.UpdateCharacterStats(characterStats, statModifiers);

            //Assert
            Assert.AreEqual(25, characterStats.FirstOrDefault(t => t.StatType == statType).Amount);
        }
    }
}
